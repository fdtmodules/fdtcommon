﻿using System;
using System.Collections.Generic;
using com.FDT.Common.Registrables;
using com.FDT.Common.ReloadedScriptableObject;
using UnityEngine;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:12:07
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     Basic building block of scriptableObject based systems, provide equal overrides that return
    ///                  equals when two objects of this type has the same name and type. Useful to deal with
    ///                  scriptableObjects what are in asset bundles and need to appear as the same object
    ///                  to the application.
    /// ChangeLog:       2020-10-12 - Fixed usage of hashcode instead of guid for some comparisons. This fixes
    ///                               usage in dictionaries.
    /// </summary>
    [CreateAssetMenu(menuName = "FDT/Common/ID Asset", fileName = "IDAsset")]
    public class IDAsset : ScriptableObjectWithDescription, IRegistrableID, IEquatable<IDAsset>,
        IEqualityComparer<IDAsset>, IResetOnStop, IResetOnPlay
    {
                private int _guid = -1;

        public virtual void ResetValues()
        {
            _guid = -1;
        }

        public int guid
        {
            get
            {
                if (_guid == -1)
                {
                    _guid = name.GetHashCode() * GetType().GetHashCode();     
                }
                return _guid;
            }
        }
        
        bool IEquatable<IDAsset>.Equals(IDAsset other)
        {
            return Equals(other);
        }
        public static bool operator ==(IDAsset obj1, IDAsset obj2)
        {
            if (ReferenceEquals(null, obj1)) 
            {
                return ReferenceEquals(null, obj2);
            }
            else
            {
                if(ReferenceEquals(null, obj2)) 
                {
                    return false;
                }
                else
                {
                    return obj1.guid == obj2.guid;
                }
            }
        }

        private string _name = string.Empty;
        public new string name
        {
            get
            {

                if (Application.isEditor)
                {
                    return base.name;
                }
                else
                {
                    if (string.IsNullOrEmpty(_name))
                    {
                        _name = base.name;
                    }
                    return _name;
                }
            }
            set
            {
                base.name = value;
                if (!Application.isEditor)
                {
                    _name = value;
                }
            }
        }

        public static bool operator !=(IDAsset obj1, IDAsset obj2)
        {
            return !(obj1 == obj2);
        }

        private bool Equals(IDAsset other)
        {
            return other != null && guid == other.guid;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((IDAsset) obj);
        }

        public override int GetHashCode()
        {
            return guid;
        }

        public bool Equals(IDAsset x, IDAsset y)
        {
            return (y != null && x != null && x.guid == y.guid) || (x == null && y == null);
        }

        public int GetHashCode(IDAsset obj)
        {
            return guid;
        }
		
		public virtual void ResetData()
        {
            _guid = -1;
        }
    }
}
