using com.FDT.Common.Subscribe;
using UnityEngine;

namespace defaultNamespace
{
    /// <summary>
    /// Creation Date:   3/4/2021 10:43:29 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// Changelog:         
    /// </summary>
    public class SubscribedItem : MonoBehaviour, ISubscribable
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Properties, Consts and Statics
        
        #endregion

        #region Variables
        
        #endregion

        #region Public API
        
        #endregion
                        
        #region Methods
        //[ContextMenu("call from context menu")]
        private void OnEnable()
        {
            SubscriberSingleton.Instance.Register(this);
        }

        private void OnDisable()
        {
            SubscriberSingleton.Instance.Unregister(this);
        }

        //public void Test() { }        
        #endregion
    }
}