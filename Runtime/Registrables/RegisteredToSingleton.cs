﻿using UnityEngine;

namespace com.FDT.Common.Registrables
{
    /// <summary>
    /// Creation Date:   09/02/2020 17:39:47
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class RegisteredToSingleton<TRegisterer, 
        TRegistrable, TRegistrableId> : RegisteredBase<TRegistrableId>,
        IRegistrable<TRegistrableId>
        where TRegisterer : Singleton<TRegisterer>, IRegisterer<TRegistrable, TRegistrableId>
        where TRegistrable : class, IRegistrable<TRegistrableId>
        where TRegistrableId : IRegistrableID
    {
        #region Methods
        protected override void Register()
        {
            if (Singleton<TRegisterer>.Instance.Register(((Object) this) as TRegistrable))
            {
                OnRegisteredSuccess();
            }
            else
            {
                OnRegisteredFailed();
            }
        }
        protected override void Unregister()
        {
            if (Singleton<TRegisterer>.Exists)
            {
                if (Singleton<TRegisterer>.Instance.Unregister(((Object) this) as TRegistrable))
                {
                    OnUnregisteredSuccess();
                }
                else
                {
                    OnUnregisteredFailed();
                }
            }
        }
        #endregion
    }
    
}