﻿using UnityEngine;

namespace com.FDT.Common.Registrables
{
    /// <summary>
    /// Creation Date:   09/02/2020 17:39:47
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class RegisteredToScriptableObject<TRegisterer, 
        TRegistrable, TRegistrableId> : RegisteredBase<TRegistrableId>,
        IRegistrable<TRegistrableId>
        where TRegisterer : IRegisterer<TRegistrable, TRegistrableId>
        where TRegistrable : class, IRegistrable<TRegistrableId>
        where TRegistrableId : IRegistrableID
    {
        #region Inspector Fields
        [Header("Protected"), SerializeField] protected TRegisterer _registerList;
        #endregion
 
        #region Properties, Consts and Statics

        #endregion
 
        #region Methods
        protected override void Unregister()
        {
            if (_registerList.Unregister(((Object) this) as TRegistrable))
            {
                OnRegisteredSuccess();
            }
            else
            {
                OnRegisteredFailed();
            }
        }

        protected override void Register()
        {
            if (_registerList.Register(((Object) this) as TRegistrable))
            {
                OnUnregisteredSuccess();
            }
            else
            {
                OnUnregisteredFailed();
            }
        }
        #endregion
    }
}