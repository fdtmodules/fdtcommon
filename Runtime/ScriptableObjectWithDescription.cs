﻿using UnityEngine;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// </summary>
    public abstract class ScriptableObjectWithDescription : ScriptableObject
    {
        protected virtual string CustomDescription
        {
            get { return null; }
        }
        [SerializeField, Description("CustomDescription")] protected string _description;
    }
}