using System;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   10/4/2020 2:33:28 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class InterfaceObjectBase
    {
        #region Properties, Consts and Statics
        public virtual Type FilterType
        {
            get;
        }
        public bool Changed
        {
            set => _changed = true;
        }
        #endregion

        #region Variables
        protected bool _changed = false;
        #endregion
    }
}