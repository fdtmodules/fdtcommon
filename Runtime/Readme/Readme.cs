using UnityEngine;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   4/14/2020 12:17:48 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class Readme : MonoBehaviour
    {
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("GameEvents"), SerializeField] 
        //[Header("UnityEvents"), SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        [TextArea(3, 25), SerializeField] protected string _description;
        [SerializeField] protected Color _color;
        //[Header("Protected"), SerializeField]
        //[Header("Private"), SerializeField]

        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        #endregion

        #region Methods

        void Reset()
        {
            _color = Color.gray;
        }
        #endregion
    }
}