namespace com.FDT.Common.ReloadedScriptableObject
{
    /// <summary>
    /// Creation Date:   24/02/2020 17:22:06
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public interface IStateRestore:ISaveable
    {
    }
}