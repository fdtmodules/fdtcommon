﻿using UnityEngine;

namespace com.FDT.Common
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Common
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class PropertyAttributeBase : PropertyAttribute 
	{
		public string label;
		public string tooltip;
	}
}