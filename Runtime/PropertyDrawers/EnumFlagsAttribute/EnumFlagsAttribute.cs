﻿using System;

namespace com.FDT.Common
{
    /// <summary>
    /// Creation Date:   01/02/2020 22:51:22
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     Save and shows several values in a bitmask type enum.
    /// </summary>
	[AttributeUsage(AttributeTargets.Field, Inherited = true)]
	public class EnumFlagsAttribute : PropertyAttributeBase
	{
		public EnumFlagsAttribute() { }
	}
}