using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 20:43:19
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class InstancedPropertyDrawer<T, TViewData> : PropertyDrawer where T:class where TViewData:InstancedPropertyDrawer<T,TViewData>.ViewDataBase, new()
    {
        public class ViewDataBase
        {
            public T cTarget;
        }

        protected TViewData viewData;
        private Dictionary<string, TViewData> _viewDatas = new Dictionary<string, TViewData>();
		
        #region Properties, Consts and Statics
		
        #endregion
        #region Methods

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            GetViewData(property);
            DoOnGUI(position, property, label);
        }

        protected virtual void DoOnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
           
        }
        public void GetViewData(SerializedProperty property)
        {
            if (!_viewDatas.TryGetValue(property.propertyPath, out viewData))
            {
                viewData = new TViewData();
                _viewDatas[property.propertyPath] = viewData;
                viewData.cTarget = ReflectionExtensions.GetTargetObjectOfProperty(property) as T;
            }
            OnGetViewData(property, viewData);
        }

        protected virtual void OnGetViewData(SerializedProperty property, TViewData viewDataBase)
        {
			
        }

        #endregion
    }
}