using com.FDT.Common.Editor;
using UnityEditor;

namespace com.FDT.Common.TemplateCustomization.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 22:37:31
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// </summary>
    public class TemplateEditorWindow : EditorWindow
    {
        #region Variables
        private static string namespaceText;
        private static string productName;
        private static string companyName;
        private static string devName;
        private static string scriptableMenu;
        #endregion
        #region Methods
        [MenuItem("Window/Template Options...", false, 15)]
        static void CreateWindow()
        {
            TemplateEditorWindow options = GetWindow<TemplateEditorWindow>(true, "Template Options", true);
            options.Show();
            Init();
        }
        static void Init()
        {
            namespaceText = TemplateCustomization.GetNamespaceText();
            productName = TemplateCustomization.GetProductName();
            companyName = TemplateCustomization.GetCompanyName();
            devName = TemplateCustomization.GetDevName();
            scriptableMenu = TemplateCustomization.GetScriptMenu();
        }

        void OnGUI()
        {
            EditorExtensions.Title("All Scripts");
            EditorGUI.BeginChangeCheck();
            namespaceText = EditorGUILayout.TextField("Namespace", namespaceText);
            FailReason namespaceError = namespaceText.ValidNamespace();
            if (EditorGUI.EndChangeCheck() && namespaceError == FailReason.NONE)
            {
                EditorPrefs.SetString(TemplateCustomization.NamespaceTextVar, namespaceText);
            }

            if (namespaceError != FailReason.NONE)
            {
                string failError = EditorExtensions.GetNamespaceErrorString(namespaceError);
                EditorGUILayout.HelpBox(failError, MessageType.Error);
            }
            
            EditorGUI.BeginChangeCheck();
            productName = EditorGUILayout.TextField("Product Name:", productName);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.ProductNameVar, productName);
            }
            
            EditorGUI.BeginChangeCheck();
            companyName = EditorGUILayout.TextField("Company Name:", companyName);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.CompanyNameVar, companyName);
            }
            
            EditorGUI.BeginChangeCheck();
            devName = EditorGUILayout.TextField("Developer Name:", devName);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.DevNameVar, devName);
            }
            
            EditorExtensions.Title("ScriptableObjects");
            
            EditorGUI.BeginChangeCheck();
            scriptableMenu = EditorGUILayout.TextField("Creation menu path:", scriptableMenu);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetString(TemplateCustomization.ScriptableMenuVar, scriptableMenu);
            }
        }
        #endregion
    }
}