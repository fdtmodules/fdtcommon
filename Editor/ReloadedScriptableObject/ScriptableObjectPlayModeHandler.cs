﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace com.FDT.Common.ReloadedScriptableObject.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 22:08:34
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// </summary>
    [InitializeOnLoad, DefaultExecutionOrder(-999)]
    public static class ScriptableObjectPlayModeHandler
    {
        private static List<IMonoState> _monostate;
        private static List<IStateRestore> _stateRestore;
        private static List<IResetOnPlay> _resetOnPlay;
        private static List<IResetOnStop> _resetOnStop;
        private static readonly Dictionary<ISaveable, string> _savedData = new Dictionary<ISaveable, string>();
        
        #region Methods

        static ScriptableObjectPlayModeHandler()
        {
            EditorApplication.playModeStateChanged += HandleStateChanged;
        }

        private static void HandleStateChanged(PlayModeStateChange state)
        {
            if (state == PlayModeStateChange.ExitingEditMode || state == PlayModeStateChange.EnteredEditMode)
            {
                ResetAllMonoState();
            }
            if (state == PlayModeStateChange.ExitingEditMode)
            {
                GetData();
                ResetScripts();
            }

            if (state == PlayModeStateChange.EnteredPlayMode)
            {
                GetData();
                SaveContent();
            }
            if (state == PlayModeStateChange.ExitingPlayMode)
            {
                GetData();
                ReloadContent();
            }
        }

        private static void GetData()
        {
            if (_resetOnPlay == null)
            {
                ScriptableObject[] scriptableObjects = Resources.FindObjectsOfTypeAll<ScriptableObject>();
                _resetOnPlay = scriptableObjects.Where(a => a is IResetOnPlay).Cast<IResetOnPlay>().ToList();
                _resetOnStop = scriptableObjects.Where(a => a is IResetOnStop).Cast<IResetOnStop>().ToList();
                _stateRestore = scriptableObjects.Where(a => a is IStateRestore).Cast<IStateRestore>().ToList();
                _monostate =scriptableObjects.Where(a => a is IMonoState).Cast<IMonoState>().ToList();
            }
        }
        private static void ResetScripts()
        {
            GetData();
            for (int i = 0; i < _resetOnPlay.Count; i++)
            {
                _resetOnPlay[i].ResetValues();
            }
        }
        private static void ReloadContent()
        {
            GetData();
            for (int i = 0; i < _stateRestore.Count; i++)
            {
                if (_stateRestore[i] is ICustomStateRestore state)
                {
                    state.LoadData();
                }
                else
                {
                    var so = _stateRestore[i];
                    if (_savedData.ContainsKey(so))
                    {
                        var castType = so.GetType();
                        var o = System.Convert.ChangeType(so, castType);
                        JsonUtility.FromJsonOverwrite(_savedData[so], o);
                    }
                }
            }
            for (int i = 0; i < _resetOnStop.Count; i++)
            {
                _resetOnStop[i].ResetValues();
            }
        }

        private static void SaveContent()
        {
            GetData();
            for (int i = 0; i < _stateRestore.Count; i++)
            {
                if (_stateRestore[i] is ICustomStateRestore state)
                {
                    state.SaveData();
                }
                else
                {
                    var so = _stateRestore[i];
                    var castType = so.GetType();
                    var o = System.Convert.ChangeType(so, castType);
                    if (!_savedData.ContainsKey(so))
                    {
                        _savedData.Add(so, String.Empty);
                    }
                    _savedData[so] = JsonUtility.ToJson(o);
                }
            }
        }
        private static void ResetAllMonoState()
        {
            GetData();
            for (int i = 0; i < _monostate.Count; i++)
            {
                _monostate[i].ResetValues();
            }
        }
        #endregion
    }
}