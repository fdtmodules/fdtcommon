﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace com.FDT.Common.Editor
{
    /// <summary>
    /// Creation Date:   23/02/2020 20:48:35
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:
    /// </summary>
    public static class EditorExtensions
    {
        #region Properties, Consts and Statics
        private static readonly string[] reserved = {
                "abstract",
                "as",
                "base",
                "bool",
                "break",
                "byte",
                "case",
                "catch",
                "char",
                "checked",
                "class",
                "const",
                "continue",
                "decimal",
                "default",
                "delegate",
                "do",
                "double",
                "else",
                "enum",
                "event",
                "explicit",
                "extern",
                "false",
                "finally",
                "fixed",
                "float",
                "for",
                "foreach",
                "goto",
                "if",
                "implicit",
                "in",
                "int",
                "interface",
                "internal",
                "is",
                "lock",
                "long",
                "namespace",
                "new",
                "null",
                "object",
                "operator",
                "out",
                "override",
                "params",
                "private",
                "protected",
                "public",
                "readonly",
                "ref",
                "return",
                "sbyte",
                "sealed",
                "short",
                "sizeof",
                "stackalloc",
                "static",
                "string",
                "struct",
                "switch",
                "this",
                "throw",
                "true",
                "try",
                "typeof",
                "uint",
                "ulong",
                "unchecked",
                "unsafe",
                "ushort",
                "using",
                "virtual",
                "void",
                "volatile",
                "while"
            };
        #endregion
        #region Methods

        public static bool IsEditingInPrefabMode(this GameObject target)
        {
            if (EditorUtility.IsPersistent(target))
            {
                return true;
            }
            else
            {
                var mainStage = StageUtility.GetMainStageHandle();
                var currentStage = StageUtility.GetStageHandle(target);
                if (currentStage != mainStage)
                {
                    var prefabStage = PrefabStageUtility.GetPrefabStage(target);
                    if (prefabStage != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsAssetOnDisk(this GameObject target)
        {
            return PrefabUtility.IsPartOfPrefabAsset(target) || target.IsEditingInPrefabMode();
        } 
        public static bool IsValidNamespace(this string nspace)
        {
            return nspace.ValidNamespace() != FailReason.NONE;
        }
        public static FailReason ValidNamespace(this string nspace)
        {
            if (nspace.Contains("..")) return FailReason.DOUBLE_DOT;
            if (nspace.Contains(" ")) return FailReason.SPACE;
            if (nspace.EndsWith(".")) return FailReason.ENDSWITH_DOT;
            if (nspace.StartsWith(".")) return FailReason.STARTSWITH_DOT;
            
            var s = nspace.ToLower(CultureInfo.InvariantCulture);

            for (int i = 0; i < reserved.Length; i++)
            {
                if (s.StartsWith(reserved[i] + "."))
                {
                    return FailReason.CONTAINS_RESERVED;
                }
                if (s.Contains("." + reserved[i] + "."))
                {
                    return FailReason.CONTAINS_RESERVED;
                }
                if (s.EndsWith("." + reserved[i]))
                {
                    return FailReason.CONTAINS_RESERVED;
                }
            }
            return FailReason.NONE;
        }
        public static float GetHeight(this SerializedProperty p)
        {
            return EditorGUI.GetPropertyHeight(p);
        }
        public static Type GetTypeFromName(string propName)
        {
            var all = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (var a in all)
            {
                var e = a.GetType(propName);
                if (e != null)
                    return e;
                var allTypes = a.GetTypes();
                foreach (var t in allTypes)
                {
                    if (t.Name == propName)
                    {
                        return t;
                    }
                }
            }
            return null;
        }
        
        
        public static string GetNamespaceErrorString(FailReason namespaceError)
        {
            switch (namespaceError)
            {
                case FailReason.SPACE:
                    return "Invalid namespace: Space character detected.";
                case FailReason.UNKNOWN:
                    return "Invalid namespace: Unknown reason.";
                case FailReason.DOUBLE_DOT:
                    return "Invalid namespace: Double dot detected.";
                case FailReason.ENDSWITH_DOT:
                    return "Invalid namespace: Ends with a dot.";
                case FailReason.STARTSWITH_DOT:
                    return "Invalid namespace: Starts with a dot.";
                case FailReason.CONTAINS_RESERVED:
                    return "Invalid namespace: Contains reserved word.";
            }
            return "Invalid namespace: Unknown error.";
        }

        public static void CreateScriptAsset(string templatePath, string destName) 
        {
        #if UNITY_2019_1_OR_NEWER
            UnityEditor.ProjectWindowUtil.CreateScriptAssetFromTemplateFile(templatePath, destName);
        #else
	        typeof(UnityEditor.ProjectWindowUtil)
		        .GetMethod("CreateScriptAsset", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic)
		        .Invoke(null, new object[] { templatePath, destName });
        #endif
        }
       
        public static void GUIDrawSprite(Vector2 guiPos, Sprite sprite, float size = 1)
        {
            float w = sprite.rect.width * size;
            float h = sprite.rect.height * size;
            Rect r = new Rect(guiPos.x - (w*0.5f), guiPos.y-(h*0.5f),w,h);
            var targetSprite = sprite;
            Rect spr = targetSprite.rect;
            Vector2 totalTextureDimensions = new Vector2(targetSprite.texture.width, targetSprite.texture.height);
            GUI.DrawTextureWithTexCoords(r, targetSprite.texture, new Rect(spr.x / totalTextureDimensions.x, spr.y / totalTextureDimensions.y, spr.width / totalTextureDimensions.x, spr.height / totalTextureDimensions.y));
        }
        public static void GUIDrawSprite(Vector3 worlPos, Sprite sprite, float size = 1)
        {
            Vector2 p = HandleUtility.WorldToGUIPoint(worlPos);
            GUIDrawSprite(p, sprite, size);
        }
        private static Dictionary<Type, Dictionary<Type, bool>> subclassRawGenericCache = new Dictionary<Type, Dictionary<Type, bool>>();

        public static void Title(string title) {
            GUIContent t = new GUIContent(title);
            Title(t);
        }
        public static void Title(GUIContent title) {
            Rect position = GUILayoutUtility.GetRect(title, TitleStyle);
            Title(position, title);
            GUILayout.Space(-1);
        }
        public static void Title(Rect position, string title) {
            GUIContent t = new GUIContent(title);
            Title(position, t);
        }
        public static void Title(Rect position, GUIContent title) {
            if (Event.current.type == EventType.Repaint)
                TitleStyle.Draw(position, title, false, false, false, false);
        }

        private static GUIStyle TitleStyle;
        static EditorExtensions()
        {
            TitleStyle = new GUIStyle();
            TitleStyle.border = new RectOffset(2, 2, 2, 1);
            TitleStyle.margin = new RectOffset(5, 5, 5, 0);
            TitleStyle.padding = new RectOffset(5, 5, 3, 3);
            TitleStyle.alignment = TextAnchor.MiddleLeft;
            TitleStyle.normal.background = EditorExtensionsResources.GetTexture(EditorExtensionsResources.EditorExtensionsTexture.TitleBackground);
            TitleStyle.normal.textColor = EditorGUIUtility.isProSkin
                ? new Color(0.8f, 0.8f, 0.8f)
                : new Color(0.2f, 0.2f, 0.2f);
        }
        #endregion
    }
}