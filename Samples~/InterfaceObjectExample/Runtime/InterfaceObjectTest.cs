using UnityEngine;

namespace com.FDT.Common.Example
{
    /// <summary>
    /// Creation Date:   10/4/2020 1:42:17 PM
    /// Product Name:    FDT Common
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class InterfaceObjectTest : MonoBehaviour
    {
        
        [System.Serializable]
        public class Test:InterfaceObject<ITestable>
        {
            
        }
        #region Classes, Structs and Enums
        
        #endregion

        #region GameEvents and UnityEvents
        //[Header("")]
        //[SerializeField] 
        #endregion

        #region Actions, Delegates and Funcs
        
        #endregion

        #region Inspector Fields
        //[Header("")]
        [SerializeField] protected Test _test;

        #endregion

        #region Properties, Consts and Statics

        #endregion

        #region Variables

        #endregion

        #region Public API

        private void Update()
        {
            if (_test?.Value != null)
            {
                Debug.Log(_test.Value.a);
            }
        }

        #endregion

        #region Methods

        //[ContextMenu("call from context menu")]
        //public void Test() { }        

        #endregion
    }
}